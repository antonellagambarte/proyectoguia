$(function(){
    $("[data-toggle='tooltip']").tooltip();
    $('[data-toggle="popover"]').popover();
    $('.carousel').carousel({
      interval: 3000
    });
    $('#contacto').on('show.bs.modal', function(e){
        $('#BtnContacto').removeClass('btn-outline-success');
        $('#BtnContacto').addClass('btn-primary');
        $('#BtnContacto').prop('disabled', true);
    });
    $('#contacto').on('shown.bs.modal', function(e){
        console.log('Se esta mostró un modal');
    });
    $('#contacto').on('hide.bs.modal', function(e){
        console.log('Se oculta el modal');
    });
    $('#contacto').on('hidden.bs.modal', function(e){
        console.log('Se ocultó el modal');
        $('#BtnContacto').prop('disabled', false);
        $('#BtnContacto').removeClass('btn-primary');
        $('#BtnContacto').addClass('btn-outline-success');
        
    });
  })